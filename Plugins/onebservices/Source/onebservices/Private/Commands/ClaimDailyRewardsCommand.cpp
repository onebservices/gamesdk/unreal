// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/ClaimDailyRewardsCommand.h"

UClaimDailyRewardsCommand* UClaimDailyRewardsCommand::Create() {
    UClaimDailyRewardsCommand* Command = NewObject<UClaimDailyRewardsCommand>();
    Command->Param = "/dailyrewards/claim";
    return Command;
}