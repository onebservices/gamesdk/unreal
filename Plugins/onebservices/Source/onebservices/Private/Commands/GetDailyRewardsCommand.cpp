// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/GetDailyRewardsCommand.h"

UGetDailyRewardsCommand* UGetDailyRewardsCommand::Create() {
    UGetDailyRewardsCommand* Command = NewObject<UGetDailyRewardsCommand>();
    Command->Param = "/dailyrewards";
    return Command;
}