// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/GetGameObjectCommand.h"

UGetGameObjectCommand* UGetGameObjectCommand::Create(const FString& name){
    UGetGameObjectCommand* Command = NewObject<UGetGameObjectCommand>();
    Command->Param = "/game/"+ name;
    return Command;
}
