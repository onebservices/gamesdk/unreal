// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/GetItemTableCommand.h"

UGetItemTableCommand* UGetItemTableCommand::Create() {
    UGetItemTableCommand* Command = NewObject<UGetItemTableCommand>();
    Command->Param = "/itemtable";
    return Command;
}