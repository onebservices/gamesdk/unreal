// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/GetMyRankLeaderboardCommand.h"

UGetMyRankLeaderboardCommand* UGetMyRankLeaderboardCommand::Create(const FString& name) {
    UGetMyRankLeaderboardCommand* Command = NewObject<UGetMyRankLeaderboardCommand>();
    Command->Param = "/leaderboard/me";
    Command->Query = "?name=" + name;
    return Command;
}