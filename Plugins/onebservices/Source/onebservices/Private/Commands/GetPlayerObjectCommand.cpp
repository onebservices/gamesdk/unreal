// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/GetPlayerObjectCommand.h"

UGetPlayerObjectCommand* UGetPlayerObjectCommand::Create(const FString& name){
    UGetPlayerObjectCommand* Command = NewObject<UGetPlayerObjectCommand>();
    Command->Param = "/player/"+ name;
    return Command;
}
