// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/GetTopLeaderboardCommand.h"

UGetTopLeaderboardCommand* UGetTopLeaderboardCommand::Create(const FString& name, const FString& country, const int offset, const int count) {
    UGetTopLeaderboardCommand* Command = NewObject<UGetTopLeaderboardCommand>();
    Command->Param = "/leaderboard";
    Command->Query = "?name=" + name +"&offset=" + FString::FromInt(offset)+"&count="+ FString::FromInt(count)+"&country="+ country;
    return Command;
}