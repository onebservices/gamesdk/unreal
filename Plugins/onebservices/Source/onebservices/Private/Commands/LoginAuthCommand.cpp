// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/LoginAuthCommand.h"
#include "json.h"


ULoginAuthCommand* ULoginAuthCommand::Create(const FString& playerId,const FString& secretKey,const FString& playerName,const FString& country){
    ULoginAuthCommand* LoginAuthCommand = NewObject<ULoginAuthCommand>();
    LoginAuthCommand->Param = "/auth/login";
    LoginAuthCommand->RequestVerb = ERequestVerb::POST;
    LoginAuthCommand->JsonBody = MakeShared<FJsonObject>();
    LoginAuthCommand->JsonBody->SetStringField("playerId",playerId);
    LoginAuthCommand->JsonBody->SetStringField("secretKey", secretKey);
    LoginAuthCommand->JsonBody->SetStringField("playerName", playerName);
    LoginAuthCommand->JsonBody->SetStringField("country", country);
    return LoginAuthCommand;
}
