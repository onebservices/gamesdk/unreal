// Fill out your copyright notice in the Description page of Project Settings.


#include "Commands/PostPlayerObjectCommand.h"

UPostPlayerObjectCommand* UPostPlayerObjectCommand::Create(const FString& name, const TSharedPtr<FJsonObject>& JsonObject)
{
    UPostPlayerObjectCommand* Command = NewObject<UPostPlayerObjectCommand>();
    Command->Param = "/player/"+ name;
    Command->RequestVerb = ERequestVerb::POST;
    if (JsonObject.IsValid())
    {
        Command->JsonBody = JsonObject.ToSharedRef();
    }
    return Command;
}