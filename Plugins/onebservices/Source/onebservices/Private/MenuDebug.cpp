// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuDebug.h"
#include "Components/Button.h"
#include "Engine/GameInstance.h"
#include "OneBServicesSubsystem.h"
#include "Commands/LoginAuthCommand.h"
#include "Commands/GetPlayerObjectCommand.h"
#include "Commands/GetGameObjectCommand.h"
#include "Commands/PostPlayerObjectCommand.h"
void UMenuDebug::MenuSetup(FString GameId, EGameEnvironment GameEnvironment, FString GameVersion ) {
	AddToViewport();
	SetVisibility(ESlateVisibility::Visible);
	bIsFocusable = true;

	UWorld* World = GetWorld();
	if (World) {
		APlayerController* PlayerController = World->GetFirstPlayerController();
		if (PlayerController) {
			FInputModeUIOnly InputModeData;
			InputModeData.SetWidgetToFocus(TakeWidget());
			InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			PlayerController->SetInputMode(InputModeData);
			PlayerController->SetShowMouseCursor(true);
		}
	}
	UGameInstance* GameInstance = GetGameInstance();
	if (GameInstance) {
		OneBServicesSubsystem = GameInstance->GetSubsystem<UOneBServicesSubsystem>();
        if (OneBServicesSubsystem) {
            OneBServicesSubsystem->Init(GameId, GameEnvironment, GameVersion);
        }
	}
}
bool UMenuDebug::Initialize() {
	if (!Super::Initialize()){
		return false;
	}
	if (LoginButton) {
		LoginButton->OnClicked.AddDynamic(this, &ThisClass::LoginButtonClicked);
	}
    
    if (GetPlayerProfileButton) {
        GetPlayerProfileButton->OnClicked.AddDynamic(this, &ThisClass::GetPlayerProfileButtonClicked);
    }
    if (GetPlayerDataButton) {
        GetPlayerDataButton->OnClicked.AddDynamic(this, &ThisClass::GetPlayerDataButtonClicked);
    }
    if (GetGameDataButton) {
        GetGameDataButton->OnClicked.AddDynamic(this, &ThisClass::GetGameDataButtonClicked);
    }
    if (PostPlayerDataButton) {
        PostPlayerDataButton->OnClicked.AddDynamic(this, &ThisClass::PostPlayerDataButtonClicked);
    }
    
	return true;
}

void UMenuDebug::LoginButtonClicked() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString(TEXT("Login Button Clicked")));
	}
    if (OneBServicesSubsystem) {
        SendRequestCompleteDelegate.BindDynamic(this, &ThisClass::OnSendRequestComplete);
        OneBServicesSubsystem->SendRequest(ULoginAuthCommand::Create("tdtanvn","bimat","Tu Dinh Tan","Viet Nam"), SendRequestCompleteDelegate);
    }
}
void UMenuDebug::GetPlayerProfileButtonClicked() {
    if (OneBServicesSubsystem) {
        SendRequestCompleteDelegate.BindDynamic(this, &ThisClass::OnSendRequestComplete);
        OneBServicesSubsystem->SendRequest(UGetPlayerObjectCommand::Create("PROFILE"), SendRequestCompleteDelegate);
    }
}

void UMenuDebug::GetPlayerDataButtonClicked() {
    if (OneBServicesSubsystem) {
        SendRequestCompleteDelegate.BindDynamic(this, &ThisClass::OnSendRequestComplete);
        OneBServicesSubsystem->SendRequest(UGetPlayerObjectCommand::Create("DATA"), SendRequestCompleteDelegate);
    }
}
void UMenuDebug::GetGameDataButtonClicked() {
    if (OneBServicesSubsystem) {
        SendRequestCompleteDelegate.BindDynamic(this, &ThisClass::OnSendRequestComplete);
        OneBServicesSubsystem->SendRequest(UGetGameObjectCommand::Create("DATA"), SendRequestCompleteDelegate);
    }
}
void UMenuDebug::PostPlayerDataButtonClicked() {
    if (OneBServicesSubsystem) {
        SendRequestCompleteDelegate.BindDynamic(this, &ThisClass::OnSendRequestComplete);
        TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();
        JsonObject->SetStringField("customField", "new string");
        JsonObject->SetNumberField("level", 12);
        OneBServicesSubsystem->SendRequest(UPostPlayerObjectCommand::Create("DATA", JsonObject), SendRequestCompleteDelegate);
    }
}

void UMenuDebug::OnSendRequestComplete(const FResponse& Response) {
    if (GEngine) {
        FString OutputString;
        TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
        FJsonSerializer::Serialize(Response.JsonObject.ToSharedRef(), Writer);
        GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, TEXT("Response: ")+ OutputString);
    }
}
