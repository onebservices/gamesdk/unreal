// Fill out your copyright notice in the Description page of Project Settings.


#include "OneBServicesSubsystem.h"
#include "Commands/LoginAuthCommand.h"
#include "Json.h"

FString UOneBServicesSubsystem::AccessToken = TEXT("");
UOneBServicesSubsystem::UOneBServicesSubsystem() {

}
void UOneBServicesSubsystem::Init(FString gameID, EGameEnvironment gameEnvironment, FString gameVersion) {
    GameId = gameID;
    GameEnv = gameEnvironment;
    GameVer = gameVersion;
    if (GameEnv == EGameEnvironment::PRODUCTION) {
        UriBase = FString(PROD_URL_BASE);
    }
}
void UOneBServicesSubsystem::SendRequest(URequestCommand* RequestCommand,const FResponseDelegate& ResponseDelegate) {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString(TEXT("Verb:") + RequestCommand->GetVerbAsString()));
        GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString(TEXT("ContentType:") + RequestCommand->GetContentTypeAsString()));
        GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString(TEXT("Param:") + RequestCommand->GetParamAsString()));
        GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString(TEXT("Query:") + RequestCommand->GetQueryAsString()));
        GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString(TEXT("Body:") + RequestCommand->GetBodyAsString()));
    }
    
    FString uriRequest = UriBase + RequestCommand->GetParamAsString() + RequestCommand->GetQueryAsString();

    TSharedRef<IHttpRequest, ESPMode::ThreadSafe> HttpRequest = FHttpModule::Get().CreateRequest();
    HttpRequest->SetVerb(RequestCommand->GetVerbAsString());
    if(RequestCommand->IsA(ULoginAuthCommand::StaticClass())){
        TSharedPtr<FJsonObject> JsonBody = RequestCommand->GetBody().ToSharedRef();
        JsonBody->SetStringField("gameId",GameId);
        JsonBody->SetStringField("gameVersion",GameVer);
    }else{
        HttpRequest->SetHeader(TEXT("Authorization"), FString(TEXT("Bearer ")+ AccessToken));
    }
    if (RequestCommand->GetVerb() != ERequestVerb::GET){
        HttpRequest->SetHeader(TEXT("Content-Type"), RequestCommand->GetContentTypeAsString());
        HttpRequest->SetContentAsString(RequestCommand->GetBodyAsString());
    }
    HttpRequest->SetURL(uriRequest);
    
    HttpRequest->OnProcessRequestComplete().BindWeakLambda(this,
              [ResponseDelegate, RequestCommand](FHttpRequestPtr HttpRequest, const FHttpResponsePtr HttpResponse, const bool bWasSuccessful) {

                int32 ResponseCode = HttpResponse->GetResponseCode();
                if (!bWasSuccessful || !HttpResponse.IsValid())
                {
                    //UE_LOG("OneBServices", Error, TEXT("Request failed (%d): %s"), ResponseCode, *Request->GetURL());
                    return;
                }
                const TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(*HttpResponse->GetContentAsString());
                TSharedPtr<FJsonObject> JsonObject;
                if (FJsonSerializer::Deserialize(Reader, JsonObject))
                {
                    if(RequestCommand->IsA(ULoginAuthCommand::StaticClass())){
                        JsonObject->TryGetStringField("accessToken", AccessToken);
                    }
                    ResponseDelegate.ExecuteIfBound(FResponse(JsonObject));
                }
        });
    HttpRequest->ProcessRequest();
}

void UOneBServicesSubsystem::Test() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Yellow, FString(TEXT("Call test")));
	}
}
