// Fill out your copyright notice in the Description page of Project Settings.


#include "RequestCommand.h"

FString URequestCommand::GetBodyAsString()
{
    if(JsonBody.IsValid()){
        FString OutputString;
        TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
        FJsonSerializer::Serialize(JsonBody.ToSharedRef(), Writer);
        return OutputString;
    }
    return "";

}
FString URequestCommand::GetParamAsString()
{
    return Param;
}
FString URequestCommand::GetVerbAsString(){
    switch (RequestVerb) {
        case ERequestVerb::GET:
            return TEXT("GET");
        case ERequestVerb::POST:
            return TEXT("POST");
        case ERequestVerb::PUT:
            return TEXT("PUT");
        default:
            return TEXT("");
    }
}
FString URequestCommand::GetQueryAsString(){
    return Query;
}
FString URequestCommand::GetContentTypeAsString(){
    switch (RequestContentType) {
        case ERequestContentType::x_www_form_urlencoded_url:
            return TEXT("application/x-www-form-urlencoded");
        case ERequestContentType::x_www_form_urlencoded_body:
            return TEXT("application/x-www-form-urlencoded");
        case ERequestContentType::json:
            return TEXT("application/json");
        default:
            return TEXT("");
    }
}
ERequestVerb URequestCommand::GetVerb() {
    return RequestVerb;
}

TSharedPtr<FJsonObject>& URequestCommand::GetBody() {
    return JsonBody;
}