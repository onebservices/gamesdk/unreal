// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RequestCommand.h"
#include "CheckDailyRewardsCommand.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UCheckDailyRewardsCommand : public URequestCommand
{
    GENERATED_BODY()
public:
    static UCheckDailyRewardsCommand* Create();
};
