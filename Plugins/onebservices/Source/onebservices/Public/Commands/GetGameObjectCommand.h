// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RequestCommand.h"
#include "GetGameObjectCommand.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UGetGameObjectCommand : public URequestCommand
{
    GENERATED_BODY()
public:
    static UGetGameObjectCommand* Create(const FString& name);
};
