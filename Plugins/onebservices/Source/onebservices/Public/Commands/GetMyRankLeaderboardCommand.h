// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RequestCommand.h"
#include "GetMyRankLeaderboardCommand.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UGetMyRankLeaderboardCommand : public URequestCommand
{
    GENERATED_BODY()
public:
    static UGetMyRankLeaderboardCommand* Create(const FString& name);
};
