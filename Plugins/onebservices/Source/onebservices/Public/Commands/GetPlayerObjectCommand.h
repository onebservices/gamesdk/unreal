// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RequestCommand.h"
#include "UObject/NoExportTypes.h"
#include "GetPlayerObjectCommand.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UGetPlayerObjectCommand : public URequestCommand
{
    GENERATED_BODY()
public:
    static UGetPlayerObjectCommand* Create(const FString& name);
};
