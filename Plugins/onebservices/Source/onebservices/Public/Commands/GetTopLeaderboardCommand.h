// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RequestCommand.h"
#include "GetTopLeaderboardCommand.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UGetTopLeaderboardCommand : public URequestCommand
{
    GENERATED_BODY()
public:
    static UGetTopLeaderboardCommand* Create(const FString& name,const FString& country=TEXT(""), const int offset = 0, const int count = 50);
};
