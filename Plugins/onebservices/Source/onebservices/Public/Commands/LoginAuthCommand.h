// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RequestCommand.h"
#include "UObject/NoExportTypes.h"
#include "LoginAuthCommand.generated.h"
/**
 * 
 */
UCLASS()
class ONEBSERVICES_API ULoginAuthCommand: public URequestCommand
{
    GENERATED_BODY()
public:
    static ULoginAuthCommand* Create(const FString& playerId,const FString& secretKey,const FString& playerName=FString(TEXT("")),const FString& country=FString(TEXT("")));
};
