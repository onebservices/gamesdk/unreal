// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MarkDeleteInboxMessageCommand.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UMarkDeleteInboxMessageCommand : public UObject
{
	GENERATED_BODY()
	
};
