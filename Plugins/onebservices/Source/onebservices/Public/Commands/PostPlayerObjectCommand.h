// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RequestCommand.h"
#include "PostPlayerObjectCommand.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UPostPlayerObjectCommand : public URequestCommand
{
    GENERATED_BODY()

public:
    static UPostPlayerObjectCommand* Create(const FString& name, const TSharedPtr<FJsonObject>& JsonObject);
};
