// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OneBServicesSubsystem.h"
#include "MenuDebug.generated.h"

/**
 * 
 */
UCLASS()
class ONEBSERVICES_API UMenuDebug : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
    void MenuSetup(FString GameId=FString(TEXT("DEMO")),EGameEnvironment GameEnvironment = EGameEnvironment::DEVELOPMENT,FString GameVersion=FString(TEXT("")) );

protected:
	virtual bool Initialize() override;

	UFUNCTION()
	void OnSendRequestComplete(const FResponse& Response);
    
    FResponseDelegate SendRequestCompleteDelegate;
private:

	UPROPERTY(meta = (BindWidget))
	class UButton* LoginButton;
    
    UPROPERTY(meta = (BindWidget))
    UButton* GetPlayerProfileButton;

    UPROPERTY(meta = (BindWidget))
    UButton* GetPlayerDataButton;
    
    UPROPERTY(meta = (BindWidget))
    UButton* PostPlayerDataButton;
    
    UPROPERTY(meta = (BindWidget))
    UButton* GetGameDataButton;

	UFUNCTION()
		void LoginButtonClicked();
    
    UFUNCTION()
        void GetPlayerProfileButtonClicked();
    
    UFUNCTION()
        void GetPlayerDataButtonClicked();
    
    UFUNCTION()
        void PostPlayerDataButtonClicked();
    
    UFUNCTION()
        void GetGameDataButtonClicked();
    
	class UOneBServicesSubsystem* OneBServicesSubsystem;

};
