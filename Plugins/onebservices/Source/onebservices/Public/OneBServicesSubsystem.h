// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "RequestCommand.h"
#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "OneBServicesSubsystem.generated.h"



/**
 * 
 */

DECLARE_DYNAMIC_DELEGATE_OneParam(FResponseDelegate,const FResponse&, Response);

#define PROD_URL_BASE TEXT("https://api.1bservices.com")
#define DEV_URL_BASE TEXT("https://dev.api.1bservices.com")

UENUM()
enum class EGameEnvironment : uint8
{
    DEVELOPMENT,
    PRODUCTION
};


UCLASS()
class ONEBSERVICES_API UOneBServicesSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	UOneBServicesSubsystem();
	void Init(FString gameID, EGameEnvironment gameEnvironment = EGameEnvironment::DEVELOPMENT, FString gameVersion = FString(TEXT("")));
	void SendRequest(URequestCommand* RequestCommand, const FResponseDelegate& ResponseDelegate);
	void Test();

private:
	FString GameId;
	FString GameVer;
    EGameEnvironment GameEnv;
    FString UriBase{FString(DEV_URL_BASE)};
    static FString AccessToken;
};
