// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "json.h"
#include "RequestTypes.h"
#include "RequestCommand.generated.h"
/**
 * 
 */
UCLASS()
class ONEBSERVICES_API URequestCommand: public UObject
{
    GENERATED_BODY()
public:
    FString GetBodyAsString();
    FString GetParamAsString();
    FString GetVerbAsString();
    FString GetQueryAsString();
    FString GetContentTypeAsString();
    ERequestVerb GetVerb();
    TSharedPtr<FJsonObject>& GetBody();
    
protected:
    ERequestVerb RequestVerb;
    ERequestContentType RequestContentType{ERequestContentType::json};
    FString Param{TEXT("")};
    FString Query{TEXT("")};
    TSharedPtr<FJsonObject> JsonBody;
};
